//package com.rpg.temp;
//
//import java.util.ArrayList;
//
//import com.badlogic.gdx.Gdx;
//import com.badlogic.gdx.graphics.Texture;
//import com.badlogic.gdx.graphics.g2d.Sprite;
//import com.badlogic.gdx.graphics.g2d.SpriteBatch;
//import com.badlogic.gdx.math.Vector2;
//import com.badlogic.gdx.physics.box2d.Body;
//import com.badlogic.gdx.physics.box2d.BodyDef;
//import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
//import com.badlogic.gdx.physics.box2d.FixtureDef;
//import com.badlogic.gdx.physics.box2d.PolygonShape;
//import com.rpg.objects.Being;
//import com.rpg.objects.GameWorld;
//import com.rpg.objects.Obstacle;
//import com.rpg.objects.Player;
//import com.rpg.objects.Wolf;
//
//public class LevelTest extends GameWorld {
//	private Body ground;
//	private Body leftEdge;
//	private Body RightEdge;
//	
//	private Sprite sprite;
//	private SpriteBatch batch;
//	private Texture groundText;
//	
//	//Obstacle list functioning as the level itself
//	private ArrayList<Obstacle> obstacleList;
//
//	public LevelTest() {
//		// TODO Auto-generated constructor stub
//		super();
//		
//	}
//
//	@Override
//	public void create() {
//		//Aspect ratio scaling test
//		batch = new SpriteBatch();
//		groundText = new Texture(Gdx.files.internal("level1.png"));
//		sprite = new Sprite(groundText, 0, groundText.getHeight()-38, 480, 38);
//		
//		
//		//Initialize every object here
//		obstacleList = new ArrayList<Obstacle>(); //List for easy access to every obstacle
//		
//		//Ground
//		obstacleList.add(new Obstacle(0, 0, getWorld(),(float)getWidth(), 20f, 0.0f, null));
//		//Walls
//		obstacleList.add(new Obstacle(20f, 80f, getWorld(),20f, 40f, 0.0f, null));
//		//Small obstacles
//		obstacleList.add(new Obstacle(300f, 30f, getWorld(),5f, 10f, 0.0f, null));
//			
//		Player player = new Player(200f, 100f);
//		player.setBody(getWorld());
//		//player.getShape().setRadius(60f);
//		player.getFixtureDef().density = 10f;
//		player.getFixtureDef().friction = 1.0f;
//		player.setFixture();
//		
//
//		this.setPlayer(player);
//		Wolf wolf = new Wolf(200f,100f);
//		wolf.setBody(getWorld());
//		wolf.getShape().setRadius(10f);
//		wolf.getFixtureDef().density = 1.0f;
//		wolf.getFixtureDef().friction = 1.0f;
//		wolf.setFixture();
//		BodyDef grounddef = new BodyDef();
//		grounddef.position.set(new Vector2(Gdx.graphics.getWidth()/2,0));
//		grounddef.type = BodyType.StaticBody;
//		FixtureDef groundfixdef = new FixtureDef();
//		PolygonShape shape = new PolygonShape();		
//		shape.setAsBox(Gdx.graphics.getWidth()/2, 1f);			
//		groundfixdef.shape = shape;		
//		groundfixdef.density = 1.0f;
//		groundfixdef.friction = 1.0f;
//		
//		ground = this.getWorld().createBody(grounddef);
//		ground.createFixture(groundfixdef);		
//		shape.dispose();		
//		this.addBeing(wolf);
//		this.addBeing(player);
//		
//	}
//	
//	@Override
//	public void update(){
//		ArrayList<Being> beings = this.getBeings();
//		Player player = this.getPlayer();
//		for(Being being : beings) {
//			being.update(player);
//		}
//		batch.begin();
//		sprite.draw(batch);
//		batch.end();
//		
//		//Run all update() methods for every object in the level here
//	}
//	
//	@Override
//	public void dispose() {
//		// TODO Auto-generated method stub
//		for(Obstacle o: obstacleList){
//			o.dispose();
//		}
//	}
//	
//	public void maintainAspectRatio(){
//		float width = Gdx.graphics.getWidth();
//		float height = Gdx.graphics.getHeight();
//		
//		float sharedWidth = width / sprite.getWidth();
//		float  sharedHeight = height / sprite.getHeight();
//		
//		sprite.setSize(sprite.getWidth()*sharedWidth, sprite.getHeight()*(height/width));
//		
//				
//	}
//
//
//	@Override
//	public Sprite getBgSprite() {
//		// TODO Auto-generated method stub
//		return null;
//	}
//
//}
