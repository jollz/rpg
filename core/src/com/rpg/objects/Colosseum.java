package com.rpg.objects;

import static com.rpg.helpers.UnitConverter.PPM;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.rpg.helpers.SpriteScaler;
import com.rpg.screens.GameScreen;


/**
 * A Colosseum level in the game.
 * This class extends the class GameWorld.java
 * to specify the specifics of objects and
 * settings to be used in the Colosseum level.
 * 
 * @author Daniel Ronnefjord
 *
 */
public class Colosseum extends GameWorld {
	private Random rng = new Random();
	private Sprite leftWallSprite, rightWallSprite, groundSprite, bgSprite;
	private Texture texture, bgTexture;
	private final float UIOffset = (Gdx.graphics.getHeight()/PPM)/4;
	private Music moshPit = Gdx.audio.newMusic(Gdx.files.internal("music/foeglnColoss_mp3.mp3"));
	private Shroomster shroomster;
	private float spawnTimer = 10f;
	private int killCount;
	
	/**
	 * Constructor, calls the constructor in it's super class.
	 */
	public Colosseum(){
		super();
	}
	
	/**
	 * The create method initializes all objects
	 * and sprites that will be used in the game.
	 * 
	 * Initial spawns of monsters and players is
	 * made here.
	 * 
	 * Set up for various settings will take place
	 * here aswell.
	 */
	@Override
	public void create() {
		//The level's texture
		texture = new Texture(Gdx.files.internal("level1.png"));
		texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		bgTexture = new Texture(Gdx.files.internal("Background/bglvl1.png"));
		bgTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		
		//The level's Sprite's
		bgSprite = new Sprite(bgTexture);
		SpriteScaler.scale(bgSprite);
		bgSprite.setPosition(Gdx.graphics.getWidth()/2-(bgSprite.getWidth()/2), (Gdx.graphics.getHeight()/2-(bgSprite.getHeight()/2))+UIOffset+30);
		groundSprite = new Sprite(texture, 0, texture.getHeight()-39, 480, 39);
		SpriteScaler.scale(groundSprite);
		leftWallSprite = new Sprite(texture, 0, texture.getHeight()-131, 35, 131);
		SpriteScaler.scale(leftWallSprite);
		rightWallSprite = new Sprite(texture, texture.getWidth()-35, texture.getHeight()-131, 35, 131);
		SpriteScaler.scale(rightWallSprite);
		
		//Static bodies for the actual level blocks
		this.addObstacle(new Obstacle(groundSprite.getWidth()/2, groundSprite.getHeight()/2 + UIOffset, getWorld(), groundSprite.getWidth()/2, groundSprite.getHeight()/2, 1, groundSprite));
		this.addObstacle(new Obstacle(leftWallSprite.getWidth()/2, leftWallSprite.getHeight()/2 + UIOffset, getWorld(), leftWallSprite.getWidth()/2, leftWallSprite.getHeight()/2 + 2, 1, leftWallSprite));
		this.addObstacle(new Obstacle((Gdx.graphics.getWidth()/PPM) - rightWallSprite.getWidth()/2, rightWallSprite.getHeight()/2 + UIOffset, getWorld(), rightWallSprite.getWidth()/2, rightWallSprite.getHeight()/2 + 2, 1, rightWallSprite));
		
		//Player initialization
		Player player = new Player((Gdx.graphics.getWidth()/2-100)/PPM, (Gdx.graphics.getHeight()/2+20)/PPM);
		player.setBody(getWorld());
		player.getFixtureDef().density = 1f;
		player.getFixtureDef().friction = 1f;
		player.setFixture();
		player.setEnemies(this.getBeings());
		this.setPlayer(player);
		this.addBeing(player);
		
		//Initiate monsters or w/e
		shroomster = new Shroomster((Gdx.graphics.getWidth()/2)/PPM, (Gdx.graphics.getHeight()/2+200)/PPM, this.getPlayer());
		shroomster.setBody(getWorld());
		shroomster.getFixtureDef().density = 0.5f;
		shroomster.getFixtureDef().restitution = 0.1f;
		shroomster.setFixture();
		this.addBeing(shroomster);
		
		//Colosseum's gravity
		getWorld().setGravity(new Vector2(0f, -9.82f));
		moshPit.setLooping(true);
		moshPit.play();
		}
	
	/**
	 * The update method gets called once per frame.
	 * It iterates through all obejcts and updates them.
	 * 
	 * Removes monsters that have died or objects that
	 * has been destroyed.
	 * 
	 * Also updates current statistics for the player
	 * and steps the physics world.
	 */
	@Override
	public void update() {
		for(Being b : this.getBeings()){
			b.update();
			if(b.isDead()){
				killCount++;			
			}
		}
		super.killBeings();
		
		Label scoreLabel = GameScreen.ih.getScoreLabel();
		String currentScore = "Score: " + killCount*10;
		scoreLabel.setText(currentScore);
		
		spawnShrooms();
		
		getWorld().step(Gdx.graphics.getDeltaTime(), 6, 2);
	}
	
	private void spawnShrooms(){
		spawnTimer -= Gdx.graphics.getDeltaTime();
		if(spawnTimer <= 0){
			for(int i = 0; i <= killCount; i++){
				Shroomster shroomster = new Shroomster((Gdx.graphics.getWidth()/2 * (rng.nextFloat() + 0.5f))/PPM, (Gdx.graphics.getHeight()/2+300)/PPM, this.getPlayer());
				shroomster.setBody(getWorld());
				shroomster.getFixtureDef().density = 0.5f;
				shroomster.getFixtureDef().restitution = 0.1f;
				shroomster.setFixture();
				this.addBeing(shroomster);
				spawnTimer = 10.0f;
			}
		}
	}
	
	/**
	 * Disposes all objects to free up the memory being
	 * being used by them.
	 */
	@Override
	public void dispose() {
		moshPit.stop();
		moshPit.dispose();
		for(Obstacle obst : this.getObstacles()){
			obst.dispose();
		}
	}
	
	/**
	 * Getter for the background of the level.
	 */
	@Override
	public Sprite getBgSprite() {
		return bgSprite;
	}
	
	/**
	 * Getter for the portal object, if one
	 * is present.
	 */
	@Override
	public Portal getPortal() {
		return null;
	}
}
