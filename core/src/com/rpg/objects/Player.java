package com.rpg.objects;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.rpg.helpers.State;
import com.rpg.screens.GameScreen;

public class Player extends Being {
	private float posX, posY;
	private List<Being> enemies = new ArrayList<Being>();
	private float speed = 5f;
	private int damage = 25;
	private int xp = 0;
	private Texture texture;
	private int level = 1;
	private int NEXT_LEVEL = 20;
	public static ShapeRenderer shapeRenderer;
	private Sound jumpSound = Gdx.audio.newSound(Gdx.files.internal("sounds/Jump.wav"));
	private Sound attackSound = Gdx.audio.newSound(Gdx.files.internal("sounds/attack.wav"));
	private Music levelupSound = Gdx.audio.newMusic(Gdx.files.internal("sounds/levelup.mp3"));
	private float radius; 
	private float distance; //Maximum distance between player and finger before camera stops moving
	
	
	/**
	 * 
	 * @param posX the player's starting xposition
	 * @param posY the player's starting ypositon
	 */
	public Player(float posX, float posY){
		super(BodyType.DynamicBody, posX, posY, 5f, 0.4f);
		FixtureDef f = this.getFixtureDef();
		f.filter.categoryBits = Being.CATEGORY_PLAYER;	
		f.filter.groupIndex = -77;
		//super.setAttackAnim(setUpAnimation());
		this.setSpeed(speed);
		
		
	}
	
	@Override
	/** updates the player in every frame **/
	public void update(){
		NEXT_LEVEL = 75 + (25 * level);
		
		movePlayer();
		
		Label hplabel = GameScreen.ih.getHpLabel();
		String currentText = "Health: " + this.getHp() + "/" + this.getMaxHp();
		hplabel.setText(currentText);
		Label xplabel = GameScreen.ih.getXpLabel();
		xplabel.setText("XP: " + this.getXP() + " / " + this.NEXT_LEVEL);
		Label levelLabel = GameScreen.ih.getLevelLabel();
		levelLabel.setText("Level: " + this.getLevel());
		super.update();
		//try to level up
		this.levelUp();
	}	
	
	
	/**
	 * 
	 * @return xp
	 */
	public int getXP() {
		return this.xp;
	}
	
	/**
	 * sets the xp
	 * @param xp
	 */
	public void setXp(int xp){
		this.xp = xp;
	}
	
	/**
	 * reduces xp by amount
	 * @param amount
	 */
	public void reduceXP(int amount) {
		this.xp = this.xp - amount;
	}
	
	/**
	 * increases xp by amount
	 * @param amount
	 */
	public void increaseXP(int amount){
		this.xp = this.xp + amount;
	}
	
	
	
	@Override
	public void setUpAnimations() {
		/* -- The player's texture -- */
		texture = new Texture(Gdx.files.internal("neander.png"));
		texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		
		/* -- Relevant arrays for different animations -- */
		TextureRegion[] allFrames;
		
		TextureRegion[] idle = new TextureRegion[1];
		TextureRegion[] jump = new TextureRegion[1];
		TextureRegion[] walk = new TextureRegion[4];
		TextureRegion[] attacking = new TextureRegion[3];
		
		/* -- Number of rows and columns in the texture -- */
		int row = 2;
		int col = 6;
		
		/* -- Split the texture and populate arrays -- */
		TextureRegion[][] temp = TextureRegion.split(texture, texture.getWidth()/col, texture.getHeight()/row);
		allFrames = new TextureRegion[col*row];
		
		int cnt = 0;
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				allFrames[cnt++] = temp[i][j];
			}
		}
		
		idle[0] = allFrames[0];
		jump[0] = allFrames[1];
		
		int j = 0;
		for(int i = 2; i < allFrames.length/row; i++){
			walk[j++] = allFrames[i];
		}
		j = 0;
		for(int i = 6; i < allFrames.length-3; i++){
			attacking[j++] = allFrames[i];
		}
		
		/* -- Create animations with respective TextureRegion[] -- */
		Animation idleAnim = new Animation(0.1f, idle);
		idleAnim.setPlayMode(PlayMode.LOOP);
		Animation jumpAnim = new Animation(0.1f, jump);
		jumpAnim.setPlayMode(PlayMode.LOOP);
		Animation walkAnim = new Animation(0.1f, walk);
		walkAnim.setPlayMode(PlayMode.LOOP);
		Animation attackAnim = new Animation(0.1f, attacking);
		attackAnim.setPlayMode(PlayMode.NORMAL);
		
		/* -- Add the animations to the being -- */
		super.addAnimation(idleAnim, State.STILL);
		super.addAnimation(jumpAnim, State.JUMP);
		super.addAnimation(walkAnim, State.WALKING);
		super.addAnimation(attackAnim, State.ATTACK);
		super.addAnimation(idleAnim, State.DYING);
	}
	
	
	
	
	
	
	
	
	
	//****Get/set-methods****
	
	/** gets the amount of xp needed to reach the next level
	 * 
	 * 
	 */
	public int getNextLevel() {
		return this.NEXT_LEVEL;
	}
	
	/**gets the radius of the player's body **/
	public double getRadius(){
		return radius;
	}
	public float getDistance(){
		return distance;
	}

	public Sprite getPlayerSprite(){
		return super.getSprite();
	}
	public float getPosX(){
		return posX;
	}
	public float getPosY(){
		return posY;
	}
	
	public void setPosX(float unit){
		posX = unit;
	}
	
	/**
	 * Method that makes it possible for the one who plays the game to steer the Foegel!
	 */
	public void movePlayer() {
		if(GameScreen.ih.getRightButton().getClickListener().isPressed() || Gdx.input.isKeyPressed(Keys.D)) {
			this.setState(State.WALKING);
			this.setFacingRight(true);
		} else if (GameScreen.ih.getLeftButton().getClickListener().isPressed() || Gdx.input.isKeyPressed(Keys.A)) {
			this.setState(State.WALKING);
			this.setFacingRight(false);
		} else if(this.getState() != State.ATTACK && this.getState() != State.JUMP) {
			this.setState(State.STILL);
		}
		
		if(GameScreen.ih.getButtonA().getClickListener().isPressed() || Gdx.input.isKeyPressed(Keys.DOWN)) {
			if(!GameScreen.ih.getButtonA().isDisabled()) {
				jumpSound.play();
				jumpSound.setVolume(1, 0.1f);
				this.jump();
				this.setState(State.JUMP);
			}
			GameScreen.ih.getButtonA().setDisabled(true);
			if(this.getBody().getLinearVelocity().y == 0.0f) {
				GameScreen.ih.getButtonA().setDisabled(false);
			}
		}
		if(GameScreen.ih.getButtonB().getClickListener().isPressed() || Gdx.input.isKeyPressed(Keys.LEFT)) {
			if(this.getState() == State.STILL || this.getState() == State.WALKING){
				this.setState(State.ATTACK);
			}
		} 
	}
	
	@Override
	public void dispose() {
		attackSound.dispose();
		jumpSound.dispose();
		texture.dispose();
	}

	public void setEnemies(ArrayList<Being> beings) {
		this.enemies = beings;	
	}

	/** Player's implementation of attack-method in being **/
	public void attack() {
		attackSound.play();
		for(Being enemy : this.enemies){
			if(enemy == this){
				continue;
			}
			if(this.canAttack(enemy)){
				this.dealDamage(enemy, damage);
				if(this.isFacingRight())
					enemy.getBody().applyLinearImpulse(20f, 1f, enemy.getBody().getPosition().x, enemy.getBody().getPosition().y, true);
				else if (!(this.isFacingRight()))
					enemy.getBody().applyLinearImpulse(-20f, 1f, enemy.getBody().getPosition().x, enemy.getBody().getPosition().y, true);
			}
		}		
	}			


/** levels up the player **/
public void levelUp() {
	if(this.getXP() >=  NEXT_LEVEL)
	{
		this.setLevel(this.getLevel()+1);
		this.setXp(this.getXP() - NEXT_LEVEL);
		this.setMaxHp(this.getMaxHp() + (5 + this.getLevel()));
		this.setDamage(this.getDamage());
		this.resetHp();
		levelupSound.setVolume(0.5f);
		levelupSound.play();

	}
	}

	
private void setDamage(int newDamage){
		this.damage = newDamage;
	}
private int getDamage() {
	return this.damage;
}
private void setLevel(int newLevel){
	this.level = newLevel;
}

private int getLevel() {
	return this.level;
}


}



