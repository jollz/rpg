package com.rpg.objects;

import java.util.HashMap;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.rpg.helpers.SpriteScaler;
//import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.rpg.helpers.State;


/**
 * Being
 * All creatures be it monsters or players should derive from this class
 * @author Dan Lakss
 *
 */
public abstract class Being {
	
	
	private Map<Integer,Animation> animations;
	private int MAX_HP = 100;
	private int hp;
	//private int strength;
	private Rectangle boundingbox;
	private BodyDef bodyDef;
	private Body body;
	private PolygonShape shape = new PolygonShape();
	private FixtureDef fixtureDef;
	private Fixture fixture;
	private Sprite sprite;
	private float speed;
	private State state;
	private boolean facingRight = true;
	private Animation attackAnimation;
	private float stateTime = 0;
	private float attackTimer;
	private float attackDelay;
	final static short CATEGORY_PLAYER = 0x0001;  
	final static short CATEGORY_MONSTER = 0x0002; 
	
	/***
	 * 
	 * 
	 * @param bodyType the type of body dynamic or static
	 * @param posX starting position of the being in the x-dimension
	 * @param posY starting position of the being in the y-dimension
	 * @param speed - the speed with which the being is to move.
	 * @param attackDelay
	 */
	public Being(BodyType bodyType, float posX, float posY, float speed, float attackDelay) {
		this.animations = new HashMap<Integer,Animation>();
		this.setUpAnimations();
		this.setState(State.STILL);
		sprite = new Sprite(getAnimation(State.STILL).getKeyFrame(0));
		SpriteScaler.scale(sprite);
		shape.setAsBox((sprite.getWidth()/2), (sprite.getHeight()/2));

		bodyDef = new BodyDef();
		bodyDef.fixedRotation = true;
		setBodyType(bodyType);
		this.bodyDef.position.set(posX, posY);
		fixtureDef = new FixtureDef();
		fixtureDef.shape = this.shape;
		this.speed = speed;
		this.attackDelay = attackDelay;
		Rectangle rect = new Rectangle();
		rect.setWidth(sprite.getWidth()+0.1f);
		rect.setHeight(sprite.getHeight()+0.1f);
		this.setBoundingBox(rect);
		this.setHp(MAX_HP);
		this.resetAttackTimer();
		}
	
	/** makes the being jump
	 * 
	 */
	public void jump(){
		Gdx.app.log("Player jump", "jump()");
		
		this.getBody().applyLinearImpulse(new Vector2(0f, 5f),this.getBody().getPosition(), true);

	}
	
	/** run to update the position and animation of this being **/
	
	public void update(){
		
		if(this.getattackTimer() > 0){
			this.countDownAttackTimer(Gdx.graphics.getDeltaTime());
		}
		if(this.getState() == State.ATTACK && this.getattackTimer() > 0){
			this.setState(State.STILL);
		}
		
		Animation anim = this.getAnimation(this.getState());
		
		if(this.getBody().getLinearVelocity().y > 0.01f || this.getBody().getLinearVelocity().y < -0.01f){
			if(this.state != State.WALKING){
				this.setState(State.JUMP);
			}
		} else {
			if(this.getState() != State.WALKING && this.getState() != State.ATTACK){
				this.setState(State.STILL);
			}
		}
		if(this.getState() == State.STILL){
			this.stop();
		}
		if(this.getState() == State.ATTACK){
			if(anim.isAnimationFinished(stateTime)){
				attack();
				this.resetAttackTimer();
				this.setState(State.STILL);
			}	
		}
		
		if(!anim.isAnimationFinished(stateTime)){
			stateTime += Gdx.graphics.getDeltaTime();
		} else {
			stateTime = 0;
		}
		
		this.getSprite().setRegion(anim.getKeyFrame(stateTime));
		
		if(facingRight){
			sprite.flip(false, false);
		} else if (!facingRight){
			sprite.flip(true, false);
		}
		
		if(this.getState() == State.WALKING){
			//check here to get the correct animation
			walk(this.isFacingRight());
		}	
		
		
		
		
		this.sprite.setPosition(body.getPosition().x - sprite.getWidth()/2, body.getPosition().y - sprite.getHeight()/2);		
		this.getBoundingBox().setCenter(body.getPosition());
	}
	
	/** should load and prepare the attackanimation for the being
	 * 
	 * @return Animation an animation to run when the being attacks
	 */
	
	public abstract void setUpAnimations();
	
  /**
   * 
   * @return the body of this being
   */
	public Body getBody(){
		return body;
	}
	/**
	 *  adds this beings body to a world
	 * @param world a world to add the body to
	 */
	public void setBody(World world){
		this.body = world.createBody(bodyDef);
		this.getBoundingBox().setCenter(body.getPosition());
	}
	
	
	
	/**
	 * 
	 * @return the animations of this being
	 */
	public Map<Integer,Animation> getAnimations() {
		return this.animations;
	}
	
	/**
	 * 
	 * @param s the being's current state
	 * @return the animation 
	 */
	public Animation getAnimation(State s) {
		return this.animations.get(s.ordinal());
	}
	
	/**
	 * Adds an animation to this being for a given state
	 * @param a an animation
	 * @param s the state for this animation
	 */
	public void addAnimation(Animation a, State s){
		this.animations.put(s.ordinal(), a);
	}
	
	/**
	 * 
	 * @return the current state of this being
	 */
	public State getState() {
		return this.state;
	}
	
	/** 
	 * 
	 * sets the state of this being
	 *
	 */
	public void setState(State newstate){
		
		//can't change state if you are dying
		if(this.getState() == State.DYING){
			return;
		}
		
		//never change to the same state
		if(newstate == this.getState()){
			return;
		}
		
		if(this.getState() == State.ATTACK && newstate == State.WALKING){
			return;
		}
		this.state = newstate;
		this.stateTime = 0;
		//this.setAnimation(this.getState());
	}
	
	/** Get the position of the being
	 * as a two dimensional vector.
	 * @return this being's position in the world
	 */
	public Vector2 getPosition() {
		return this.body.getPosition();
	}
	public void setBodyType(BodyType bodyType){
		this.bodyDef.type = bodyType;
	}
	
	/** 
	 * 
	 * @return the fixturedef of this being
	 */
	public FixtureDef getFixtureDef(){	//Use this to set friction etc.
		return fixtureDef;
	}
	/**
	 * 
	 * @return the shape of this being's body
	 */
	public Shape getShape(){
		return shape;
	}
	
	/**
	 * sets the fixture of this being
	 */
	public void setFixture(){
		fixture = body.createFixture(fixtureDef);
	}
	
	/**
	 * 
	 * @return the speed of this being
	 */
	public float getSpeed() {
		return this.speed;
	}
	
	/**
	 * sets the speed of this being
	 * @param speed
	 */
	public void setSpeed(float speed){
		this.speed = speed;
	}

	
	
	/**dispose/remove this being **/ 
	public void dispose(){	//BodyDefs, bodies and fixtures don't have to be disposed, -
		shape.dispose();	//but shapes have to be.
		body.destroyFixture(fixture);
	}
	
	
	/**Stop the being **/
	public void stop() {
		body.setLinearVelocity(0,body.getLinearVelocity().y);
	}

	/** get this being to start moving with it's preset velocity 
	 *  Velocity in the y-dimension is left unchanged.
	 *  @argument right in what direction to start moving
	 *  		  set false to move left and true to move right
	 * 
	 * */
	
	/**
	 *  move the being right or left
	 * @param right true if move right false otherwise
	 */
	public void walk(boolean right){
		// here we set the body to move with the predefined speed.
		// I leave the speed in the y-dimension unchanged, although one could argue
		// as we're walking it should be set to 0.
		//set negative speed to move left
		if(!right){
			body.setLinearVelocity(-this.getSpeed(), body.getLinearVelocity().y);
		}
		//moving right
		else {
			body.setLinearVelocity(this.getSpeed(), body.getLinearVelocity().y);
		}		
	}

	/** Check if this being may attack another being 
	 *  it may attack if it's 7 pixels from being or closer
	 * @param being a being to check attack
	 * @return true if this being may attack being false otherwise
	 */
	public boolean canAttack(Being being){
		return this.getBoundingBox().overlaps(being.getBoundingBox());
	}
	
	/**
	 * Sets the boundingbox of this being
	 * @param rect 
	 */
	public void setBoundingBox(Rectangle rect){
		this.boundingbox = rect;
	}
		
	public Rectangle getBoundingBox() {
		return this.boundingbox;
		
	}
		
	/**
	 * Calculate the carthesian distance between two positons
	 * @param position1
	 * @param position2
	 * @return the distance between these points
	 */
		
	public static float calcDistanceTo( Vector2 position1, Vector2 position2){
		float distanceX = position1.x - position2.x;
		float distanceY = position1.y -position2.y;
		float distance = (float) Math.sqrt((distanceX*distanceX)+(distanceY*distanceY));
		return distance;
		
	}
	
	/**
	 * get the sprite of this being
	 * @return sprite
	 */
	public Sprite getSprite(){
		return sprite;
	}
	

		/** Get the correct frame for the correct animation
		 * of this being
		 * @return a frame
		 */
		public TextureRegion getTexture() {
				
				return attackAnimation.getKeyFrame(stateTime);
		
		}
		
		/**
		 * gets the health points of this being
		 * 
		 */
		public int getHp(){
			return this.hp;
		}
		
		
		/**
		 * returns true iff this being is dead (i.e it's hp is less than 1)
		 * @return
		 */
		public boolean isDead() {
			return this.getHp() < 1;
		}

		/***
		 * Deals damage to another being
		 * @param  the being to deal damage to
		 */
		public void dealDamage(Being b, int value){
			b.takeDamage(value);			
		}
		
	
	/**
	 * resets the hp of this being to max
	 */
	public void resetHp() {
		this.setHp(MAX_HP);
	}
	
	public void setMaxHp(int newMax){
		this.MAX_HP = newMax;
	}
	public void setHp(int hp){
		this.hp = hp;
	}
	
	/**
	 * 
	 * @return the maximum hp of this being
	 */
	public int getMaxHp() {
		return this.MAX_HP;
	}
	
	
	
	
	/**
	 * Take damage
	 * @param amount the amount of damage to take
	 */
	private void takeDamage(int  amount) {
			
		this.setHp(this.getHp() - amount);
		
		if(this.isDead()){
			this.setState(State.DYING);
		}
			
		}
	/**
	 * sets the sprite of this being
	 * @param sprite
	 */
	public void setSprite(Sprite s){
		this.sprite = s;
	}
	
	public boolean isFacingRight(){
		return facingRight;
	}
	public void setFacingRight(boolean b){
		facingRight = b;
	}

	/* gets the attackTimer for this being */
	private float getattackTimer(){
		return this.attackTimer;
	}
	
	/* resets the attacktimer */
	private void resetAttackTimer(){
		this.attackTimer = attackDelay;
	}

	private void countDownAttackTimer(float amount){
		attackTimer = attackTimer - amount;
	}


	
	public float getStateTime(){
		return stateTime;
	}

	/** should be implemented by specific beings
	 * called to attack other beings
	 */
	public abstract void attack();
	
}



