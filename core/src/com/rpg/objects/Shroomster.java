package com.rpg.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.Animation.PlayMode;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.rpg.helpers.State;


/**
 * A Shroomster monster in the game.
 * This class extends the class Being.java
 * to specify the specifics of a Shroomster
 * monster.
 * 
 * @author Daniel Ronnefjord
 *
 */
public class Shroomster extends Being {
	
	private Texture texture;
	private TextureRegion[] frames;
	private int xpamount = 20;
	private Animation idleAnim;
	private Animation walkAnim;
	
	private final int row = 1;
	private final int col = 4;
	
	private Player player;
	
	private int damage = 3;
	
	/**
	 * Constructor for the Shroomster
	 * @param posX - The X coordinate where this Shroomster should spawn.
	 * @param posY - The Y coordinate where this Shroomster should spawn.
 	 * @param player - The Player the Shroomster is currently trying to kill.
	 */
	public Shroomster(float posX, float posY, Player player) {
		super(BodyType.DynamicBody, posX,posY, 0.5f, 1);
		this.getFixtureDef().filter.groupIndex = -77;
		this.player = player;
	}
	
	/**
	 * This method initializes and sets up all the animations
	 * for the Shroomster class.
	 * 
	 * It also assigns said animations to a list in the superclass
	 * Being.java
	 */
	@Override
	public void setUpAnimations() {
		// LOAD TEXTURE CONTAINING ALL ANIMATION FRAMES
		texture = new Texture(Gdx.files.internal("shroomster.png"));
		// SPLIT THE TEXTURE AND POPULATE AN ARRAY WITH THE FRAMES
		TextureRegion[][] temp = TextureRegion.split(texture, texture.getWidth()/col, texture.getHeight()/row);
		frames = new TextureRegion[col*row];
		int cnt = 0;
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				frames[cnt++] = temp[i][j];
			}
		}
		TextureRegion[] idle = new TextureRegion[1];
		idle[0] = frames[0];
		TextureRegion[] walk = new TextureRegion[3];
		int j = 0;
		for(int i = 1; i < frames.length; i++){
			walk[j] = frames[i];
			j++;
		}
		
		// INITIALIZE AN ANIMATION WITH THE FRAMES
		idleAnim = new Animation(0.08f, idle);
		idleAnim.setPlayMode(PlayMode.LOOP);
		walkAnim = new Animation(0.08f, walk);
		walkAnim.setPlayMode(PlayMode.LOOP);
		
		// ASSIGN ANIMATIONS TO CORRECT STATE
		super.addAnimation(idleAnim, State.STILL);
		super.addAnimation(walkAnim, State.WALKING);
		super.addAnimation(idleAnim, State.JUMP);
		super.addAnimation(idleAnim, State.ATTACK);
		super.addAnimation(idleAnim, State.DYING);
	}
	
	/**
	 * Updates the state for the Shroomster based
	 * on what has been going on in the game.
	 * 
	 */
	@Override
	public void update(){
		super.update();
		float distance = this.getPosition().x - player.getPosition().x;
		if(this.getState() != State.ATTACK){
			if(this.canAttack(player)){
				this.setState(State.ATTACK);
				return;
			}
			if(distance > 0){
				this.setFacingRight(false);
				if(Math.abs(distance) < 7.2f){
					this.setState(State.WALKING);
				} else {
					this.setState(State.STILL);
				}
			} else if(distance < 0) {
				this.setFacingRight(true);
				if(Math.abs(distance) < 7.2f){
					this.setState(State.WALKING);
				} else {
					this.setState(State.STILL);
				}
			}
		}
	}
	
	/**
	 * The method used when a Shroosmter is close enough
	 * to a Player and can attack.
	 */
	@Override
	public void attack() {
		this.dealDamage(player, damage);
	}
	
	/**
	 * Getter for the amount of experience a Shroomster is worth.
	 * @return - The amount of experience.
	 */
	public int getXP() {
		return this.xpamount;
	}	
}
