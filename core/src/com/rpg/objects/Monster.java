package com.rpg.objects;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.rpg.helpers.State;

public class Monster extends Being {

	private float stateTime = 0f;
	private Player player;
	
	

	public void setPlayer(Player p){
		this.player = p;
	}
	public Player getPlayer(){		
		return this.player;
	}
	public Monster(float posX, float posY) {
		super(BodyType.DynamicBody, posX,posY, 5f, 10f);
		this.setState(State.STILL);
		FixtureDef def = this.getFixtureDef();
		
		//def.filter.categoryBits = Being.CATEGORY_MONSTER;
		def.filter.groupIndex = -77;
		//System.out.println("Anropas!!!");
		setUpAnimations();
		//super.setAttackAnim(this.setUpAnimation());
	}
	
	
	
	
	
	/**
	 * Creates the animation to run when the monster attacks 
	 */
	public void setUpAnimations() {
		
		
		
		ArrayList<TextureRegion> list = new ArrayList<TextureRegion>();
		for(int i = 2 ; i < 25 ; i++){
			
			if( i == 24){
				//yes this is strange...
				continue;
			}
			// get a path to each individual sprite
			
			String path = "attack/troll_"+i+".png";
			
			if(path.equals("attack/troll_10.png")){
				//path = "attack/troll_10_v2.png";
				}
				
			//System.out.println(path);
			
			// Animation needs TextureRegions
			TextureRegion r = new TextureRegion(new Texture(Gdx.files.internal(path)));
			list.add(r);	
			
			}
		
		
		TextureRegion walk = list.get(0);
		TextureRegion[] frames = new TextureRegion[1];
		frames[0] = walk;
		Animation walkAnimation = new Animation(60f,frames);
		super.addAnimation(walkAnimation, State.WALKING);
		//super.addAnimation(walkAnimation, State.WALKING_RIGHT);
		super.addAnimation(walkAnimation, State.STILL);
		
		
		// create an array of the individual sprites
		frames = new TextureRegion[list.size()];
		list.toArray(frames);
		
		
		//Create animation
		Animation animation = new Animation(0.100f,frames);
		super.addAnimation(animation, State.ATTACK);
		
		
		
		//return null;
	
	}
	
	
	
	/** update implementation for the monster
	 * uses very simple "algorithm" to determine the monster's next action
	 * update in being is then called to execute the action
	 * @param being a being to potentially attack
	 */
	public void update(Being being) {
		
		//get the current position of the player
		Vector2 playerpos = being.getPosition();
		
			//wait if the player is dead
			if(being.isDead()){
				this.setState(State.STILL);
			}
				
			
			//if you can attack the player go into attack-state
			else if(canAttack(being)){	
				this.stop();
				
				this.setState(State.ATTACK);
				//this.setState(State.STILL);
			}
			// else locate the player and follow him
			else if(playerpos.x > this.getPosition().x) {
					this.setState(State.WALKING);
					this.setFacingRight(true);
				}
			
			else {
					this.setState(State.WALKING);
					this.setFacingRight(false);
			}
		
			//the superclass being determines action based on state, finds the apropriate animation
			// for this state and shows the correct texture for it.
			super.update();	
			
			
			
			
			
			
			
	}
			
			
		public void attack() {
			System.out.println("Wolves attack anropas!");
			if(this.canAttack(this.getPlayer()))
				this.dealDamage(this.getPlayer(), 0);
		}
			
	}
		
		
		



	
	
	
