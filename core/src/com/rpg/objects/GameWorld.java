package com.rpg.objects;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

/**
 * GameWorld
 * Abstract class to define shell of each level
 * Each level should be treated as a GameWorld object and they share most -
 * methods and fields using this class.
 * Box2D sees 1 unit as 1m
 * @author Dan Lakss
 *
 */
public abstract class GameWorld {
	private int width = Gdx.graphics.getWidth(), height = Gdx.graphics.getHeight(); //Used to set the viewport of the camera in relation to level size
	private World world; //World container holding all box2d based bodies
	private Vector2 gravityVector = new Vector2(0f, -10f); //Vector2 is an argument passed to World instances
	private ArrayList<BodyDef> bodyDefList;
	Player player;
	//Debug fields
	private Box2DDebugRenderer b2Debug; //Used to display bodies without any art being rendered unto them
	//Lists
	private ArrayList<Being> beings;
	private ArrayList<Obstacle> obstacles;
	private ArrayList<Sprite> spriteList;
	private Portal portal;
	
	public GameWorld() {
		// TODO Auto-generated constructor stub
		world = new World(gravityVector, true); //Creates the world and tells all objects to sleep
			//(conserves resources)
		b2Debug = new Box2DDebugRenderer();
		bodyDefList = new ArrayList<BodyDef>();
		beings = new ArrayList<Being>();
		obstacles = new ArrayList<Obstacle>();
		spriteList = new ArrayList<Sprite>();
		player = new Player(200f, 100f);
		player.setEnemies(beings);
	}
	public abstract Portal getPortal();
	public abstract void create();
	public abstract void update();
	public abstract void dispose();
	public abstract Sprite getBgSprite();
	//Get-/set-methods
	public int
	getWidth(){
		return width;
	}
	public int getHeight(){
		return height;
	}
	public Box2DDebugRenderer getDebug(){
		return b2Debug;
	}
	public World getWorld(){
		return world;
	}
	public ArrayList<BodyDef> getBodyDefList(){
		return bodyDefList;
	}
	
	public ArrayList<Being> getBeings() {
		return this.beings;
	}
	public void addBeing(Being b){
		beings.add(b);
	}
	
	public void killBeings() {
		List<Being> monsters = this.getBeings();
		Player p = this.getPlayer();
		Iterator<Being> it = monsters.iterator();
		while(it.hasNext()){
			Being monster = it.next();
			
			if(monster == player){
				continue;
			}
			if(monster.isDead()){
				player.increaseXP(25);
				monster.dispose();
				it.remove();
			}
		
		
		}
	
	}
	
	public ArrayList<Obstacle> getObstacles() {
		return this.obstacles;
	}
	
	public void addObstacle(Obstacle o){
		obstacles.add(o);
	}
	public Player getPlayer() {
		return this.player;
	}
	public void setPlayer(Player player){
		this.player = player;
	}
	public ArrayList<Sprite> getSpriteList(){
		return this.spriteList;
	}
	
	
	/**
	 * maintainAspectRatio() - NOT USED - WORKS ONLY WHEN DOWNSCALING
	 * Calculates the free space a sprite can scale over depending on screen size
	 * Bigger screen means more space to scale over
	 */
	public void maintainAspectRatio(ArrayList<Sprite> list, float width, float height){
		//float width = Gdx.graphics.getWidth();
		//float height = Gdx.graphics.getHeight();
		for(Sprite s: spriteList){
			float sharedWidth = s.getWidth() / width;
			float sharedHeight = s.getHeight() / height;
			
			s.setSize(width*(s.getWidth()/(float)480), height*(s.getHeight()/(float)270));
		}
		
	}
}
	

