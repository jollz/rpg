package com.rpg.objects;

import static com.rpg.helpers.UnitConverter.PPM;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;
import com.rpg.helpers.SpriteScaler;

/**
 * This is an level that you can walk around in and fight mobs that might drop a portal
 * to another level(Colosseum)
 * @author Viktor J
 */
public class ExplorerLevel extends GameWorld {
	private Sprite spriteBottom, spriteBottom1, spriteBottom2, spriteBottom3, 
	spriteLeft, spriteRight, 
	lPlatForm, lPlatForm1, lPlatForm2, lPlatForm3, lPlatForm4, lPlatForm5, lPlatForm6, lPlatForm7,
	sPlatForm, sPlatForm1, sPlatForm2, sPlatForm3, sPlatForm4, sPlatForm5, sPlatForm6, sPlatForm7,
	sPlatForm8, sPlatForm9, sPlatForm10, sPlatForm11, sPlatForm12, sPlatForm13, sPlatForm14 ;
	private Texture texture;
	private Shroomster shroomster;
	private Portal portal;
	private Hospital hospital;
	public static boolean changeLevel = false;
	private float gap = 2f;
	private Music spawnSound = Gdx.audio.newMusic(Gdx.files.internal("sounds/foegelSpawn.mp3"));
	private Music XLSound = Gdx.audio.newMusic(Gdx.files.internal("music/XLMusic.mp3"));
	
	
	private final float UIOffset = (Gdx.graphics.getHeight()/PPM)/4;
	
	/**
	 * Constructor for the Class ExplorerLevel
	 */
	public ExplorerLevel() {
		super();
	}
	
	@Override
	public void create() {
		texture = new Texture(Gdx.files.internal("level1.png"));
		texture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		spawnSound.setVolume(0.2f);
		spawnSound.play();
		XLSound.setVolume(0.4f);
		XLSound.play();
		XLSound.setLooping(true);
		
		
		// Frame Sprites
		spriteBottom = new Sprite(texture, 50, texture.getHeight()-39, 380, 39);
		SpriteScaler.scale(spriteBottom);
		spriteBottom1 = new Sprite(texture, 50, texture.getHeight()-39, 380, 39);
		SpriteScaler.scale(spriteBottom1);
		spriteBottom2 = new Sprite(texture, 50, texture.getHeight()-39, 380, 39);
		SpriteScaler.scale(spriteBottom2);
		spriteBottom3 = new Sprite(texture, 50, texture.getHeight()-39, 380, 39);
		SpriteScaler.scale(spriteBottom3);
		spriteLeft = new Sprite(texture, 0, texture.getHeight()-39, 1, 1200);
		SpriteScaler.scale(spriteLeft);
		spriteRight = new Sprite(texture, 0, texture.getHeight()-39, 1, 1200);
		SpriteScaler.scale(spriteRight);
		
		//Large PlatForm Sprites
		lPlatForm = new Sprite(texture, 50, texture.getHeight()-39, 380, 10);
		SpriteScaler.scale(lPlatForm);
		lPlatForm1 = new Sprite(texture, 50, texture.getHeight()-39, 380, 10);
		SpriteScaler.scale(lPlatForm1);
		lPlatForm2 = new Sprite(texture, 50, texture.getHeight()-39, 380, 10);
		SpriteScaler.scale(lPlatForm2);
		lPlatForm3 = new Sprite(texture, 50, texture.getHeight()-39, 380, 10);
		SpriteScaler.scale(lPlatForm3);
		lPlatForm4 = new Sprite(texture, 50, texture.getHeight()-39, 380, 10);
		SpriteScaler.scale(lPlatForm4);
		lPlatForm5 = new Sprite(texture, 50, texture.getHeight()-39, 380, 10);
		SpriteScaler.scale(lPlatForm5);
		lPlatForm6 = new Sprite(texture, 50, texture.getHeight()-39, 380, 10);
		SpriteScaler.scale(lPlatForm6);
		lPlatForm7 = new Sprite(texture, 50, texture.getHeight()-39, 380, 10);
		SpriteScaler.scale(lPlatForm7);
		
		//Small Platform Sprites
		sPlatForm = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm);
		sPlatForm1 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm1);
		sPlatForm2 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm2);
		sPlatForm3 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm3);
		sPlatForm4 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm4);
		sPlatForm5 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm5);
		sPlatForm6 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm6);
		sPlatForm7 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm7);
		sPlatForm8 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm8);
		sPlatForm9 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm9);
		sPlatForm10 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm10);
		sPlatForm11 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm11);
		sPlatForm12 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm12);
		sPlatForm13 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm13);
		sPlatForm14 = new Sprite(texture, 0, texture.getHeight()-131, 39, 10);
		SpriteScaler.scale(sPlatForm14);
		
		hospital = new Hospital();
		Obstacle obst = new Obstacle(spriteBottom.getWidth() * 3 ,(spriteBottom.getHeight()) + UIOffset + hospital.getSprite().getHeight()/2,getWorld(),
						hospital.getSprite().getWidth()/2, 						
						hospital.getSprite().getHeight()/2,1,hospital.getSprite());
						
		this.addObstacle(obst);
	
		
		//Portal to the Colosseum
		portal = new Portal();
		this.addObstacle(new Obstacle(spriteBottom.getWidth()/2, 
				(spriteBottom.getHeight()/2) + UIOffset + portal.getPortalSprite().getHeight(), getWorld(), 
				portal.getPortalSprite().getWidth()/2,  portal.getPortalSprite().getHeight()/2, 1, portal.getPortalSprite()));
		
		//Frame Obstacles
		this.addObstacle(new Obstacle(spriteBottom.getWidth()/2, (spriteBottom.getHeight()/2) + UIOffset , getWorld(), spriteBottom.getWidth()/2, spriteBottom.getHeight()/2, 1, spriteBottom));
		this.addObstacle(new Obstacle(spriteBottom1.getWidth()/2 + spriteBottom.getWidth(), spriteBottom1.getHeight()/2 + UIOffset , getWorld(), spriteBottom1.getWidth()/2, spriteBottom1.getHeight()/2, 1, spriteBottom1));
		this.addObstacle(new Obstacle(spriteBottom2.getWidth()/2 + spriteBottom.getWidth()*2, spriteBottom2.getHeight()/2 + UIOffset , getWorld(), spriteBottom2.getWidth()/2, spriteBottom2.getHeight()/2, 1, spriteBottom2));
		this.addObstacle(new Obstacle(spriteBottom3.getWidth()/2 + spriteBottom.getWidth()*3, spriteBottom3.getHeight()/2 + UIOffset , getWorld(), spriteBottom3.getWidth()/2, spriteBottom3.getHeight()/2, 1, spriteBottom3));
		this.addObstacle(new Obstacle(spriteLeft.getWidth()/2, spriteLeft.getHeight()/2, getWorld(), spriteLeft.getWidth(), spriteLeft.getHeight(), 1, spriteLeft));
		this.addObstacle(new Obstacle(spriteRight.getWidth()/2 + (1480*spriteRight.getWidth()), spriteRight.getHeight()/2, getWorld(), spriteLeft.getWidth(), spriteRight.getHeight(), 1, spriteRight));
		
		//Large Platform Obstacles
		this.addObstacle(new Obstacle(lPlatForm.getWidth()/2, lPlatForm.getHeight()/2 + ((gap * 5) + UIOffset), getWorld(), lPlatForm.getWidth()/2, lPlatForm.getHeight()/2, 1, lPlatForm));
		this.addObstacle(new Obstacle(lPlatForm1.getWidth()/2 + sPlatForm.getWidth() * 2, lPlatForm.getHeight()/2 + ((gap * 7) + UIOffset), getWorld(), lPlatForm1.getWidth()/2, lPlatForm1.getHeight()/2, 1, lPlatForm1));
		this.addObstacle(new Obstacle(lPlatForm2.getWidth()/2 + lPlatForm2.getWidth() + (sPlatForm.getWidth() * 2), lPlatForm2.getHeight()/2 + ((gap * 8) + UIOffset), getWorld(), lPlatForm2.getWidth()/2, lPlatForm2.getHeight()/2, 1, lPlatForm2));
		this.addObstacle(new Obstacle(lPlatForm3.getWidth()/2 + (sPlatForm.getWidth() * 3), lPlatForm3.getHeight()/2 + ((gap * 10) + UIOffset), getWorld(), lPlatForm3.getWidth()/2, lPlatForm3.getHeight()/2, 1, lPlatForm3));
		this.addObstacle(new Obstacle(lPlatForm4.getWidth()/2 + (lPlatForm4.getWidth() * 2) + (sPlatForm.getWidth() * 2), lPlatForm4.getHeight()/2 + ((gap * 9) + UIOffset), getWorld(), lPlatForm4.getWidth()/2, lPlatForm4.getHeight()/2, 1, lPlatForm4));
		this.addObstacle(new Obstacle(lPlatForm5.getWidth()/2 + (lPlatForm5.getWidth() * 2), lPlatForm5.getHeight()/2 + ((gap * 6) + UIOffset), getWorld(), lPlatForm5.getWidth()/2, lPlatForm5.getHeight()/2, 1, lPlatForm5));
		this.addObstacle(new Obstacle(lPlatForm6.getWidth()/2 + (lPlatForm6.getWidth() * 3), lPlatForm6.getHeight()/2 + ((gap * 10) + UIOffset), getWorld(), lPlatForm6.getWidth()/2, lPlatForm6.getHeight()/2, 1, lPlatForm6));
		this.addObstacle(new Obstacle(lPlatForm7.getWidth()/2 + lPlatForm7.getWidth() + (sPlatForm.getWidth() * 2), lPlatForm7.getHeight()/2 + ((gap * 11) + UIOffset), getWorld(), lPlatForm7.getWidth()/2, lPlatForm7.getHeight()/2, 1, lPlatForm7));
		
		//Small Platform Obstacles
		this.addObstacle(new Obstacle(sPlatForm.getWidth()/2 + lPlatForm.getWidth(), sPlatForm.getHeight()/2 + ((gap * 4) + UIOffset), getWorld(), sPlatForm.getWidth()/2, sPlatForm.getHeight()/2, 1, sPlatForm));
		this.addObstacle(new Obstacle(sPlatForm1.getWidth()/2 + (lPlatForm.getWidth() + sPlatForm.getWidth()), sPlatForm1.getHeight()/2 + ((gap * 3) + UIOffset), getWorld(), sPlatForm1.getWidth()/2, sPlatForm1.getHeight()/2, 1, sPlatForm1));
		this.addObstacle(new Obstacle(sPlatForm2.getWidth()/2 + (lPlatForm.getWidth() + sPlatForm.getWidth() * 2), sPlatForm2.getHeight()/2 + ((gap * 2) + UIOffset), getWorld(), sPlatForm2.getWidth()/2, sPlatForm2.getHeight()/2, 1, sPlatForm2));
		this.addObstacle(new Obstacle(sPlatForm3.getWidth()/2 + sPlatForm.getWidth(), sPlatForm3.getHeight()/2 + ((gap * 6) + UIOffset), getWorld(), sPlatForm3.getWidth()/2, sPlatForm3.getHeight()/2, 1, sPlatForm3));
		this.addObstacle(new Obstacle(sPlatForm4.getWidth()/2 + lPlatForm2.getWidth() + (sPlatForm.getWidth() * 3), sPlatForm4.getHeight()/2 + ((gap * 9) + UIOffset), getWorld(), sPlatForm4.getWidth()/2, sPlatForm4.getHeight()/2, 1, sPlatForm4));
		this.addObstacle(new Obstacle(sPlatForm5.getWidth()/2 + (sPlatForm.getWidth() * 2), sPlatForm5.getHeight()/2 + ((gap * 11) + UIOffset), getWorld(), sPlatForm5.getWidth()/2, sPlatForm5.getHeight()/2, 1, sPlatForm5));
		this.addObstacle(new Obstacle(sPlatForm6.getWidth()/2 + (sPlatForm.getWidth()), sPlatForm6.getHeight()/2 + ((gap * 12) + UIOffset), getWorld(), sPlatForm6.getWidth()/2, sPlatForm6.getHeight()/2, 1, sPlatForm6));
		this.addObstacle(new Obstacle(sPlatForm7.getWidth()/2, sPlatForm7.getHeight()/2 + ((gap * 13) + UIOffset), getWorld(), sPlatForm7.getWidth()/2, sPlatForm7.getHeight()/2, 1, sPlatForm7));
		this.addObstacle(new Obstacle(sPlatForm8.getWidth()/2 + (lPlatForm.getWidth() * 2) + (sPlatForm8.getWidth() * 2), sPlatForm8.getHeight()/2 + ((gap * 7) + UIOffset), getWorld(), sPlatForm8.getWidth()/2, sPlatForm8.getHeight()/2, 1, sPlatForm8));
		this.addObstacle(new Obstacle(sPlatForm9.getWidth()/2 + (lPlatForm.getWidth() * 2) + (sPlatForm9.getWidth() * 3), sPlatForm9.getHeight()/2 + ((gap * 6) + UIOffset), getWorld(), sPlatForm9.getWidth()/2, sPlatForm9.getHeight()/2, 1, sPlatForm9));
		this.addObstacle(new Obstacle(sPlatForm10.getWidth()/2 + (lPlatForm.getWidth() * 3), sPlatForm10.getHeight()/2 + ((gap * 5) + UIOffset), getWorld(), sPlatForm10.getWidth()/2, sPlatForm10.getHeight()/2, 1, sPlatForm10));
		this.addObstacle(new Obstacle(sPlatForm11.getWidth()/2 + (lPlatForm.getWidth() * 3) + sPlatForm11.getWidth(), sPlatForm11.getHeight()/2 + ((gap * 4) + UIOffset), getWorld(), sPlatForm11.getWidth()/2, sPlatForm11.getHeight()/2, 1, sPlatForm11));
		this.addObstacle(new Obstacle(sPlatForm12.getWidth()/2 + (lPlatForm.getWidth() * 3) + (sPlatForm12.getWidth() * 2), sPlatForm12.getHeight()/2 + ((gap * 3) + UIOffset), getWorld(), sPlatForm12.getWidth()/2, sPlatForm12.getHeight()/2, 1, sPlatForm12));
		this.addObstacle(new Obstacle(sPlatForm13.getWidth()/2 + (lPlatForm.getWidth() * 3) + (sPlatForm13.getWidth() * 3), sPlatForm13.getHeight()/2 + ((gap * 2) + UIOffset), getWorld(), sPlatForm13.getWidth()/2, sPlatForm13.getHeight()/2, 1, sPlatForm13));
		this.addObstacle(new Obstacle(sPlatForm14.getWidth()/2 + (lPlatForm.getWidth() * 2) + (sPlatForm.getWidth() * 2), sPlatForm14.getHeight()/2 + ((gap * 10) + UIOffset), getWorld(), sPlatForm14.getWidth()/2, sPlatForm14.getHeight()/2, 1, sPlatForm14));
		
		//Player initialization
		final float PLAYER_START_POS = gap * 17;
		player = new Player(0.3f, PLAYER_START_POS);
		player.setBody(getWorld());
		player.getFixtureDef().density = 1f;
		player.getFixtureDef().friction = 1.0f;
		player.setFixture();
		player.setEnemies(this.getBeings());
		this.setPlayer(player);
		this.addBeing(player);
		
		//XL's gravity
		getWorld().setGravity(new Vector2(0f, -9.82f));
		
		for(int i = 0; i <= 15; i++) {
			addMob(i);
		}
		
	}

	@Override
	public void update() {
		for(Being b : this.getBeings()){
			b.update();
		}
		
		super.killBeings();
		

		if(portal.checkForIntersection(this.getPlayer().getBoundingBox())) {
			dispose();
			changeLevel = true;

		}
		
		hospital.isTouched(this.getPlayer());
		
		if(this.getBeings().size() < 10) {
			for(int i = 0; i <= 10; i++) {
				addMob(i);
			}
		}
		
		getPlayer().movePlayer();
		getWorld().step(Gdx.graphics.getDeltaTime(), 8, 3);
		
	}

	@Override
	public void dispose() {
		shroomster.dispose();
		spawnSound.dispose();
		texture.dispose();
		XLSound.stop();
		XLSound.dispose();
	}

	@Override
	public Sprite getBgSprite() {
		return null;
	}
	/**
	 * This method will add a Shroomster(mob) to the level and spawn them throughout the map
	 * @param i
	 */
	public void addMob(int i) {
		Random rng = new Random();
		shroomster = new Shroomster((Gdx.graphics.getWidth()/2 * rng.nextFloat())/PPM * i, (Gdx.graphics.getHeight()/2)/PPM * (i/2), this.getPlayer());
		shroomster.setBody(getWorld());
		shroomster.getFixtureDef().density = 0.5f;
		shroomster.getFixtureDef().restitution = 0.1f;
		shroomster.setFixture();
		
		this.addBeing(shroomster);
	}
	@Override
	public Portal getPortal() {
		return portal;
	}
}
