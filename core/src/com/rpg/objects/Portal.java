package com.rpg.objects;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.rpg.helpers.SpriteScaler;


/**
 * This is a Class that makes an portal for you to travel between different levels
 * @author Viktor J
 *
 */
public class Portal {
	
	private Sprite portalSprite;
	private Texture portalTexture;
	private Obstacle obstacle;
	private Random r;
	/**
	 * Constructor for the Class Portal
	 */
	public Portal() {
		r = new Random();
		portalTexture = new Texture(Gdx.files.internal("portal.png"));
		portalSprite = new Sprite(portalTexture, portalTexture.getWidth(), portalTexture.getHeight());
		SpriteScaler.scale(portalSprite);
	}
	/**
	 * Method that checks if the player walks into the portal
	 * @param rect
	 * @return
	 */
	public boolean checkForIntersection(Rectangle rect){
		if(Intersector.overlaps(rect, portalSprite.getBoundingRectangle())){
			return true;
		}
		return false;
	}
	/**
	 * Getter for the portals sprite
	 * @return
	 */
	public Sprite getPortalSprite() {
		return portalSprite;
	}

}
