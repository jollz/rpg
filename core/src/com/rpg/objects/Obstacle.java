package com.rpg.objects;


import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;

/**
 * Obstacle
 * Static obstacles making up each level
 * Doesn't need a Fixture or FixtureDef
 * You still call createFixture() on the body and pass the shape as an argument though
 * @author Dan Lakss
 *
 */
public class Obstacle {
	private BodyDef bodyDef;
	private Body body;
	private PolygonShape shape;
	private Sprite sprite;
	
	/**
	 * Creates a new Obstacle
	 * Sets the position, world in which it resides, the box dimensions, density, the sprite being used on it.
	 * @param posX
	 * @param posY
	 * @param world
	 * @param boxWidth
	 * @param boxHeight
	 * @param density
	 * @param sprite
	 */
	public Obstacle(float posX, float posY, World world, float boxWidth, float boxHeight, float density, Sprite sprite) {
		// TODO Auto-generated constructor stub
		this.bodyDef = new BodyDef();
		this.bodyDef.type = BodyType.StaticBody;
		this.bodyDef.position.x = posX;
		this.bodyDef.position.y = posY;
		
		this.body = world.createBody(this.bodyDef);
		this.shape = new PolygonShape();
		this.shape.setAsBox(boxWidth, boxHeight);
		setFixture(density);
		
		
		this.sprite = sprite;
		sprite.setPosition(body.getPosition().x - sprite.getWidth()/2, body.getPosition().y - sprite.getHeight()/2);
	}
	
	//Get-/set-methods
	public Shape getShape(){
		return shape;
	}
	/**
	 * setFixture()
	 * Creates a fixture with the shape information and density
	 * This is called in the constructor and lets you set the density as soon as you instantiate the object
	 * @param density
	 */
	public void setFixture(float density){
		this.body.createFixture(this.shape, density);
		this.body.getFixtureList().get(0).setFriction(0.0f);
		this.body.resetMassData();
	}
	/**
	 * dispose()
	 * Dispose of everything that needs to be disposed
	 */
	public void dispose(){
		shape.dispose();
	}
	/**
	 * getSprite()
	 * Self explanatory
	 * @return
	 */
	public Sprite getSprite(){
		return sprite;
	}
}
