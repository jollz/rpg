package com.rpg.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Intersector;
import com.rpg.helpers.SpriteScaler;

public class Hospital {

	Sprite hospitalsprite;

	
	
	
	public Hospital() {
		
		Texture t = new Texture(Gdx.files.internal("shop.png"));
		
		hospitalsprite = new Sprite(t,t.getWidth(),t.getHeight());
		SpriteScaler.scale(hospitalsprite);
		
	}

	/**checked by the level to see if player interacts with this object
	 * 
	 * @param p the player
	 * @return true if the player interacts with the object
	 */
	public boolean isTouched(Player p){
		
		System.out.println("YES YES");
		
		
		if(Intersector.overlaps(p.getBoundingBox(),this.hospitalsprite.getBoundingRectangle()))
		{
			p.resetHp();
			return true;
		}
		
		else {
			return false;
		}

	}

	/*
	 * get the sprite representing this object
	 */
	public Sprite getSprite(){
		return this.hospitalsprite;
	}

	

}
