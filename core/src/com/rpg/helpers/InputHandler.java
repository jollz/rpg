package com.rpg.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;


/**
 *  @author Viktor J
 * Class to handle the input from the one who plays the game (Controllers)
 * It also makes all the Images/Buttons that is used.
 */
public class InputHandler {
	private Stage stage;
	
	private Skin tpSkin;
	
	private TouchpadStyle tpStyle;
	private Touchpad touchpad;
	
	private Drawable touchBackground;
	private Drawable touchKnob;
	
	private ImageButton buttonA;
	private ImageButton buttonB;
	private ImageButtonStyle ibsA;
	private ImageButtonStyle ibsB;
	
	private ImageButton leftButton;
	private ImageButton rightButton;
	
	private ImageButtonStyle ibsLeft;
	private ImageButtonStyle ibsRight;
	private Label hplabel, scoreLabel, xplabel, levelLabel;
	/**
	 * Creator for the class InputHandler
	 */
	public void create() {
		stage = new Stage();
		gamepad();
		buttons();
		Gdx.input.setInputProcessor(stage);
		LabelStyle style = new LabelStyle();
		style.font = new BitmapFont();
		style.fontColor = Color.RED;
		style.font.setScale(Gdx.graphics.getDensity());
		hplabel = new Label("", style);
		
		//hplabel.setSize(Gdx.graphics.getWidth()/4, Gdx.graphics.getHeight()/4);
		hplabel.setPosition(Gdx.graphics.getWidth()/2, 100);
		hplabel.setAlignment(Align.center);
		scoreLabel = new Label("", style);
		scoreLabel.setPosition(Gdx.graphics.getWidth()/2, 20);
		scoreLabel.setAlignment(Align.center);
		xplabel = new Label("",style);
		xplabel.setPosition(Gdx.graphics.getWidth()/2,60);
		xplabel.setAlignment(Align.center);
		levelLabel = new Label("",style);
		levelLabel.setPosition(Gdx.graphics.getWidth()/2,140);
		levelLabel.setAlignment(Align.center);
		
		stage.addActor(hplabel);
		stage.addActor(scoreLabel);
		stage.addActor(xplabel);
		stage.addActor(levelLabel);
	}
	/**
	 * Method that draws the gamepad to the screen
	 */
	public void draw() {
		stage.act(Gdx.graphics.getDeltaTime());
		stage.draw();
	}
	
	/**
	 * Getter for HPlabel
	 * @return
	 */
	public Label getHpLabel() {
		return this.hplabel;
	}
	/**
	 * Getter for ScoreLabel
	 * @return
	 */
	public Label getScoreLabel() {
		return this.scoreLabel;
	}
	
	/**
	 * Method that creates the gamepad buttons
	 */
	public void buttons() {
		tpSkin.add("buttonA", new Texture(Gdx.files.internal("UI/Abutton.png")));
		tpSkin.add("buttonB", new Texture(Gdx.files.internal("UI/Bbutton.png")));
		
		ibsA = new ImageButtonStyle();
		ibsA.up = tpSkin.getDrawable("buttonA");
		ibsA.down = tpSkin.getDrawable("buttonA");
		ibsB = new ImageButtonStyle();
		ibsB.up = tpSkin.getDrawable("buttonB");
		ibsB.down = tpSkin.getDrawable("buttonB");
		
		buttonA = new ImageButton(ibsA);
		buttonB = new ImageButton(ibsB);
		
		buttonB.setSize(Gdx.graphics.getWidth() / 8, Gdx.graphics.getWidth() / 8);
		buttonA.setSize(Gdx.graphics.getWidth() / 8, Gdx.graphics.getWidth() / 8);
		
		buttonA.setPosition(Gdx.graphics.getWidth() - (buttonA.getWidth()+5), 7);
		buttonB.setPosition(Gdx.graphics.getWidth() - (buttonA.getWidth() + buttonB.getWidth() + 10), 7);
		
		buttonA.addListener(new ClickListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                return true;
			}
		});
		buttonB.addListener(new ClickListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
		});
		
		stage.addActor(buttonA);
		stage.addActor(buttonB);
		
	}
	/**
	 * method that makes the Gamepad controller
	 */
	public void gamepad() {
		tpSkin = new Skin();
		tpSkin.add("leftButton", new Texture(Gdx.files.internal("UI/leftButton.png")));
		tpSkin.add("rightButton", new Texture(Gdx.files.internal("UI/rightButton.png")));
		
		ibsLeft = new ImageButtonStyle();
		ibsLeft.up = tpSkin.getDrawable("leftButton");
		ibsLeft.down = tpSkin.getDrawable("leftButton");
		
		ibsRight = new ImageButtonStyle();
		ibsRight.up = tpSkin.getDrawable("rightButton");
		ibsRight.down = tpSkin.getDrawable("rightButton");
		
		leftButton = new ImageButton(ibsLeft);
		rightButton = new ImageButton(ibsRight);
		
		leftButton.setSize(Gdx.graphics.getWidth() / 8, Gdx.graphics.getWidth() / 8);
		rightButton.setSize(Gdx.graphics.getWidth() / 8, Gdx.graphics.getWidth() / 8);
		
		leftButton.setPosition(5, 5);
		rightButton.setPosition(leftButton.getWidth()+5, 5);
		
		stage.addActor(leftButton);
		stage.addActor(rightButton);
		
		leftButton.addListener(new ClickListener(){
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
		});
		
		rightButton.addListener(new ClickListener() {
			public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
				return true;
			}
		});
		
	}
	
	/**
	 * Method that makes and adds A joyStick to the screen if wanted!
	 */
	public void joyStick() {
		tpSkin = new Skin();
		tpSkin.add("touchBackground", new Texture(Gdx.files.internal("UI/touchBackground.png")));
		tpSkin.add("touchKnob", new Texture(Gdx.files.internal("UI/touchKnob.png")));
		
	    
		TouchpadStyle tpStyle = new TouchpadStyle();
	    touchBackground = tpSkin.getDrawable("touchBackground");
	    touchKnob = tpSkin.getDrawable("touchKnob");
		
		tpStyle.background = touchBackground;
	    tpStyle.knob = touchKnob;

	    touchpad = new Touchpad(10, tpStyle);
		touchpad.setBounds(50, 40, 50, 10);
		
		stage = new Stage();
		stage.addActor(touchpad);
	}
	// Getters!!
	public ImageButton getButtonA() {
		return buttonA;
	}
	
	public Label getXpLabel() {
		return this.xplabel;
	}
	
	public Label getLevelLabel() {
		return this.levelLabel;
	}
	
	public ImageButton getButtonB() {
		return buttonB;
	}
	
	public ImageButton getLeftButton() {
		return leftButton;
	}
	
	public ImageButton getRightButton() {
		return rightButton;
	}
	public Stage getStage() {
		return stage;
	}
	
	
}
