package com.rpg.helpers;


import static com.rpg.helpers.UnitConverter.PPM;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Sprite;

/**
 * SpriteScaler
 * Takes a sprite in the scale() method and scales to the virtual width and height (480x270) while converting from pixels to meters using PPM
 * Works independent of the actual screen size
 * @author Dan Lakss
 *
 */
public class SpriteScaler {
	
	private static float width, height;

	public static void scale(Sprite sprite){
		
		width = Gdx.graphics.getWidth();
		height = Gdx.graphics.getHeight();
		sprite.setSize((width*(sprite.getWidth()/(float)480))/PPM, (height*(sprite.getHeight()/(float)270))/PPM);
		
		
	}

}
