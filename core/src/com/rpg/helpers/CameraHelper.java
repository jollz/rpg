package com.rpg.helpers;

import com.badlogic.gdx.Gdx;
import static com.rpg.helpers.UnitConverter.PPM;
import static com.rpg.helpers.UnitConverter.V_WIDTH;
import com.badlogic.gdx.graphics.Camera;

/**
 * CameraHelper
 * Sets the camera position to wanted coordinates and moves it as long as it doesn't move beyond -
 * the level boundaries 
 * @author Dan Lakss
 *
 */
public class CameraHelper {
	private static Camera camera;
	 

	public static void setCamera(Camera cam, float coordX, float coordY) {
		camera = cam;
		float minX = 0 + camera.viewportWidth/2; float maxX = 55f-camera.viewportHeight/2;
		if(!(coordX < minX) && !(coordX > maxX)){
			camera.position.set(coordX, coordY, 0);
			camera.update();
		} else if(coordX < minX) {
			camera.position.set(minX, coordY, 0);
			camera.update();
		} else if(coordX > maxX){
			camera.position.set(maxX, coordY, 0);
			camera.update();
		}
	}

}
