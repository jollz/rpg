package com.rpg.helpers;

public enum State {

	STILL,
	JUMP,
	WALKING,
	ATTACK,
	DYING
	
}
