package com.rpg.helpers;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.rpg.objects.Being;
import com.rpg.objects.GameWorld;
import com.rpg.objects.Obstacle;

/**
 * This class handles all the rendering to the screen made in the game,
 * it iterates through a list of beings and obstacles in a given world
 * and renders them out on the screen.
 * 
 * @author Daniel Ronnefjord
 *
 */
public class Renderer {
	
	private GameWorld world;
	private SpriteBatch batch;
	private OrthographicCamera camera;
	private InputHandler ih;
	private boolean debug = false;
	
	/**
	 * Constructor for the renderer.
	 * @param world - The world which is being rendered.
	 * @param camera - The camera currently in use.
	 * @param ih - The currently used input handler.
	 */
	public Renderer(GameWorld world, OrthographicCamera camera, InputHandler ih){
		this.world = world;
		this.camera = camera;
		this.ih = ih;
		batch = new SpriteBatch();
	}
	
	/**
	 * Updates the world being rendered if needed.
	 * Used when switching levels in the game.
	 * @param world
	 */
	public void setLevel(GameWorld world){
		this.world = world;
	}
	
	/**
	 * The main method.
	 * This is called once per frame, and this is where
	 * most of the Renderer functionality is written.
	 * @param camBackground - The background that should be rendered behind all objects.
	 */
	public void render(Sprite camBackground){
		if(!debug){
			Gdx.gl.glClearColor(1, 1, 1, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);			
			batch.setProjectionMatrix(camera.combined);
			batch.begin();
			camBackground.draw(batch);
			//world.getBgSprite().draw(batch);
			// -- Render obstacles
			for(Obstacle obst : world.getObstacles()){
				obst.getSprite().draw(batch);
			}
			
			// -- Render beings --
			for(Being b : world.getBeings()){
				b.getSprite().draw(batch);
			}
			batch.end();
			ih.draw();
		} else {
			Gdx.gl.glClearColor(0, 0, 0, 1);
			Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
			
			world.getDebug().render(world.getWorld(), camera.combined);
			ih.draw();
		}
	}
}
