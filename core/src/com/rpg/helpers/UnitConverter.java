package com.rpg.helpers;

import com.badlogic.gdx.Gdx;

/**
 * UnitConverter
 * Converts pixels into meters
 * Divide all your values with PPM to represent pixels as meters which Box2D uses 
 * Better yet. Use V_WIDTH & V_HEIGHT instead of Gdx.graphics.getWidth/Height() whenever you need to use the screen width and height in calculations.
 * Import as a static package (import static com.rpg.helpers.PPM, .V_HEIGHT, .V_WIDTH) to gain access to the respective variable 
 * @author Dan Lakss
 *
 */
public class UnitConverter {
	public static final float PPM = 100f;
	public static final float V_WIDTH = Gdx.graphics.getWidth()/PPM;
	public static final float V_HEIGHT = Gdx.graphics.getHeight()/PPM;

	public UnitConverter() {
		// TODO Auto-generated constructor stub
	}

}
