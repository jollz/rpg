package com.rpg.base;

import com.badlogic.gdx.Game;
import com.rpg.screens.CreditsScreen;
import com.rpg.screens.GameScreen;
import com.rpg.screens.MenuScreen;

/**
 * The main Game object of the application.
 * 
 * Extends libgdx's Game class to provide us with
 * the methods being used by libgdx to set up a new
 * game.
 * 
 * @author Daniel Ronnefjord
 *
 */
public class RPGGame extends Game {
	
	/**
	 * First method being called by the application.
	 * Sets the screen to be equal to a new MenuScreen.
	 */
	@Override
	public void create() {
		setScreen(new MenuScreen(this));
	}
	
	/**
	 * Disposes of the game to free up memory.
	 */
	@Override
	public void dispose(){
		super.dispose();
	}
	
	/**
	 * Method used to switch screens from the MenuScreen.
	 * @param index - Integer equivalent of the screen you would like to display.
	 */
	public void switchScreen(int index){
		switch(index){
			case 0: 
				setScreen(new MenuScreen(this));
				break;
			case 1:
				setScreen(new CreditsScreen(this));
				break;
			case 2:
				setScreen(new GameScreen(this));
				break;
		}
	}
}
