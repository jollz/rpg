package com.rpg.screens;

import static com.rpg.helpers.UnitConverter.V_HEIGHT;
import static com.rpg.helpers.UnitConverter.V_WIDTH;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.rpg.base.RPGGame;
import com.rpg.helpers.CameraHelper;
import com.rpg.helpers.InputHandler;
import com.rpg.helpers.Renderer;
import com.rpg.helpers.SpriteScaler;
import com.rpg.objects.Colosseum;
import com.rpg.objects.ExplorerLevel;
import com.rpg.objects.GameWorld;
import com.rpg.objects.Player;

/**
 * Game screen of the game.
 * This is where the game is actually played.
 * 
 * This is where the levels, renderer and input
 * handler are initiated.
 * 
 * @author Daniel Ronnefjord
 *
 */
public class GameScreen implements Screen {
	private GameWorld level;
	private OrthographicCamera camera;
	private Sprite cameraBackground;
	public static InputHandler ih;
	private Renderer renderer;
	private RPGGame parent;
	private Music deathSound = Gdx.audio.newMusic(Gdx.files.internal("sounds/soundOfDeath.mp3"));
	
	/**
	 * Constructor for the Game Screen.
	 * @param parent - The RPGGame object the screen is being run in.
	 */
	public GameScreen(RPGGame parent) {
		level = new ExplorerLevel();
		level.create();
		this.parent = parent;
		
		//Camera related initiation
		cameraBackground = new Sprite(new Texture(Gdx.files.internal("Background/bglvl1.png")));
		cameraBackground.getTexture().setFilter(TextureFilter.Nearest, TextureFilter.Nearest);
		SpriteScaler.scale(cameraBackground);
		cameraBackground.setSize(V_WIDTH, V_HEIGHT);
		
		camera = new OrthographicCamera();
		camera.setToOrtho(false, V_WIDTH, V_HEIGHT);
		//camera.zoom = 5.0f;
		camera.update();
		
		//Input
		ih = new InputHandler();
		ih.create();
		ih.gamepad();
		
		renderer = new Renderer(level, camera, ih);
	}
	
	/**
	 * The "main-loop" of the game.
	 * 
	 * Here we call on update on all levels and objects and
	 * also render on the renderer.
	 * 
	 * This puts it all together!
	 */
	@Override
	public void render(float delta){
		cameraBackground.setPosition(camera.position.x - V_WIDTH/2, camera.position.y - V_HEIGHT/2);
		
		renderer.render(cameraBackground);
		level.update();
		Gdx.app.log("camera pos", "camera pos: "+camera.position.x);
		if(level.getPlayer().isDead()){
			level.dispose();
			deathSound.play();
			parent.setScreen(new GameOverScreen(this.parent));
		}
		if(level.getClass() != Colosseum.class && ExplorerLevel.changeLevel) {
			ExplorerLevel.changeLevel = false;
			Player player = level.getPlayer();
			level = new Colosseum();
			level.create();
			renderer.setLevel(level);
			
		}
		if(level.getClass() != Colosseum.class)
			CameraHelper.setCamera(camera, level.getPlayer().getPosition().x, level.getPlayer().getPosition().y);	
	}
	
	/**
	 * Disposes of the objects in use by the screen
	 * to free up memory.
	 */
	@Override
	public void dispose(){
		level.dispose();
	}
	
	/**
	 * Gets called if a resize happens.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void resize(int width, int height) {
	}
	
	/**
	 * Gets called when an application gets
	 * focus again.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void show() {
	}

	/**
	 * Gets called when an application loses
	 * focus.
	 * 
	 * Does nothing at the moment
	 */
	@Override
	public void hide() {
	}
	
	/**
	 * Gets called when an application is paused.
	 * 
	 * I.e loses focus due to phone call or popup
	 * or similar.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void pause() {
	}
	
	/**
	 * Gets called when the application resumes
	 * after a pause.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void resume() {
	}
}
