package com.rpg.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.rpg.base.RPGGame;

public class GameOverScreen implements Screen {

	float scale;
	RPGGame parent;
	Stage stage;
	private float timeout = 3.0f;
	
	/** 
	 * 
	 * @param the game object that called setScreen with an instance of this object 
	 */
	public GameOverScreen(RPGGame parent) {
	this.parent = parent;	
	scale = Gdx.graphics.getDensity();
	stage = new Stage();	
	BitmapFont f = new BitmapFont();
	Label.LabelStyle  style  = new Label.LabelStyle();
	f.setColor(Color.CYAN);
	style.font = f;
	f.setScale(scale);
	Label gameOverMessage = new Label("The game has ended.\n R.I.Pieces Foegln",style);
	gameOverMessage.setAlignment(Align.center);
	gameOverMessage.setPosition((Gdx.graphics.getWidth()/2 - gameOverMessage.getWidth()/2),
			Gdx.graphics.getHeight()/2 - (gameOverMessage.getHeight()/2));
	//gameOverMessage.scaleBy(scale);
	Image bgimg = new Image(new Texture(Gdx.files.internal("main_bg.png")));		
	bgimg.setWidth(Gdx.graphics.getWidth());
	bgimg.setHeight(Gdx.graphics.getHeight());
	stage.addActor(bgimg);
	stage.addActor(gameOverMessage);
	}
	
	
	
	@Override
	public void render(float delta) {		
		stage.draw();
		timeout = timeout - delta;
		
		if(timeout <= 0){
			parent.setScreen(new MenuScreen(parent));
			this.dispose();
		}
		
		// TODO Auto-generated method stub		
	}

	@Override
	public void resize(int width, int height) {
	
		
		
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
		
	}

}
