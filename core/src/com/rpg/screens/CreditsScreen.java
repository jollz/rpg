package com.rpg.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.rpg.base.RPGGame;

/**
 * The credits in the game's class.
 * 
 * It takes care of outputting relevant
 * data while the screen is loaded.
 * 
 * @author Daniel Ronnefjord
 *
 */
public class CreditsScreen implements Screen {
	
	RPGGame game;
	
	private Stage stage;
	private Skin skin;
	private float scale;
	
	/**
	 * Constructor
	 * Initializes all text and images being used
	 * in the credits screen.
	 * @param game
	 */
	public CreditsScreen(RPGGame game){
		this.game = game;
		skin = new Skin();
		stage = new Stage();
		scale = Gdx.graphics.getDensity();
		
		Gdx.input.setInputProcessor(stage);
		
		skin.add("bgImg", new Texture(Gdx.files.internal("main_bg.png")));
		
		BitmapFont bfont = new BitmapFont();
		bfont.setScale(scale);
		skin.add("defaultFont", bfont);
		
		Pixmap pixmap = new Pixmap(100,100, Format.RGBA8888);
		pixmap.setColor(new Color(1,1,1,1));
		pixmap.fill();
		skin.add("white", new Texture(pixmap));
		
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = skin.getFont("defaultFont");
		skin.add("lblStyle", labelStyle);
		
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.down = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.checked = skin.newDrawable("white", Color.BLUE);
		textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
		textButtonStyle.font = skin.getFont("defaultFont");
		skin.add("txtBtnStyle", textButtonStyle);
		
		Image bgImage = new Image();
		bgImage.setWidth(Gdx.graphics.getWidth());
		bgImage.setHeight(Gdx.graphics.getHeight());
		bgImage.setDrawable(skin.getDrawable("bgImg"));
		bgImage.toBack();
		
		Label title = new Label("Contact us", labelStyle);
		title.setWidth(Gdx.graphics.getWidth()/2);
		title.setHeight(Gdx.graphics.getHeight()/10);
		title.setAlignment(Align.center | Align.center);
		title.setPosition(Gdx.graphics.getWidth()/2-title.getWidth()/2, Gdx.graphics.getHeight()-(title.getHeight()*2));
		
		Label headers = new Label("Tobias Ednersson\n\nDan Lakss\n\nViktor Jegerås\n\nDaniel Ronnefjord", labelStyle);
		headers.setWidth(Gdx.graphics.getWidth()*0.5f);
		headers.setHeight(Gdx.graphics.getHeight()/1.6f);
		headers.setColor(new Color(0.8f,0.8f,0.8f,1));
		headers.setAlignment(Align.left | Align.top);
		headers.setPosition(Gdx.graphics.getWidth()/2-headers.getWidth()/2,title.getY()-headers.getHeight());
		
		Label names = new Label("\nt_ednersson@hotmail.com\n\nlakss.dan@gmail.com\n\nviktor.jegeras@hotmail.com\n\ndaniel@jollz.com", labelStyle);
		names.setWidth(Gdx.graphics.getWidth()*0.5f);
		names.setHeight(Gdx.graphics.getHeight()/1.6f);
		names.setAlignment(Align.right | Align.top);
		names.setPosition(Gdx.graphics.getWidth()/2-names.getWidth()/2,title.getY()-names.getHeight());
		
		TextButton backButton = new TextButton("Back to Main", textButtonStyle);
		backButton.setWidth(Gdx.graphics.getWidth()/5);
		backButton.setHeight(Gdx.graphics.getHeight()/15);
		backButton.setPosition(Gdx.graphics.getWidth()/2-backButton.getWidth()/2,backButton.getHeight());
		
		stage.addActor(bgImage);
		stage.addActor(title);
		stage.addActor(headers);
		stage.addActor(names);
		stage.addActor(backButton);
		
		backButton.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				trigger(0);
			}
		});
	}
	private void trigger(int index){
		game.switchScreen(index);
	}
	
	/**
	 * Renders all objects on the screen once
	 * per frame.
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(Math.min(delta, 1/60f));
		stage.draw();
	}
	
	/**
	 * If a resize is present, this method will be called
	 * and will update the viewport to fit the new
	 * resolution.
	 */
	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
		BitmapFont tmp = skin.get("defaultFont", BitmapFont.class);
		tmp.setScale(scale);
	}
	
	/**
	 * Gets called when an application gets
	 * focus again.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void show() {
	}

	/**
	 * Gets called when an application loses
	 * focus.
	 * 
	 * Calls dispose before hiding the application.
	 */
	@Override
	public void hide() {
		dispose();
	}
	
	/**
	 * Gets called when an application is paused.
	 * 
	 * I.e loses focus due to phone call or popup
	 * or similar.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void pause() {
	}
	
	/**
	 * Gets called when the application resumes
	 * after a pause.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void resume() {
	}
	
	/**
	 * Disposes of the various objects
	 * in the screen.
	 */
	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}

}
