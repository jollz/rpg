package com.rpg.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;
import com.rpg.base.RPGGame;


/**
 * The Main Menu screen of the game.
 * 
 * This is the first screen a player will see when
 * launching the game.
 * 
 * It gives you options to advance to playing or watch
 * contact information.
 * 
 * @author Daniel Ronnefjord
 *
 */
public class MenuScreen implements Screen {
	
	private RPGGame game;
	
	private Stage stage;
	private Skin skin;
	private float scale;
	
	/**
	 * Constructor for the MenuScreen.
	 * 
	 * Initializes all the objects and
	 * input handles being used in the screen.
	 * 
	 * @param game - The RPGGame the screen is being used in.
	 */
	public MenuScreen(RPGGame game){
		this.game = game;
		skin = new Skin();
		stage = new Stage(new ScalingViewport(Scaling.fill, Gdx.graphics.getWidth(), Gdx.graphics.getHeight()));
		scale = Gdx.graphics.getDensity();
		
		Gdx.input.setInputProcessor(stage);
		
		skin.add("bgImg", new Texture(Gdx.files.internal("main_bg.png")));
		
		BitmapFont bfont = new BitmapFont();
		bfont.setScale(scale);
		skin.add("defaultFont", bfont);
		
		LabelStyle labelStyle = new LabelStyle();
		labelStyle.font = skin.getFont("defaultFont");
		skin.add("lblStyle", labelStyle);
		
		Pixmap pixmap = new Pixmap(100,100, Format.RGBA8888);
		pixmap.setColor(new Color(1,1,1,1));
		pixmap.fill();
		skin.add("white", new Texture(pixmap));
		
		TextButtonStyle textButtonStyle = new TextButtonStyle();
		textButtonStyle.up = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.down = skin.newDrawable("white", Color.DARK_GRAY);
		textButtonStyle.checked = skin.newDrawable("white", Color.BLUE);
		textButtonStyle.over = skin.newDrawable("white", Color.LIGHT_GRAY);
		textButtonStyle.font = skin.getFont("defaultFont");
		skin.add("txtBtnStyle", textButtonStyle);
		
		Image bgImage = new Image();
		bgImage.setWidth(Gdx.graphics.getWidth());
		bgImage.setHeight(Gdx.graphics.getHeight());
		bgImage.setDrawable(skin.getDrawable("bgImg"));
		bgImage.toBack();
		
		Label title = new Label("Foegln", labelStyle);
		title.setWidth(Gdx.graphics.getWidth()/2);
		title.setHeight(Gdx.graphics.getHeight()/10);
		title.setAlignment(Align.center | Align.center);
		title.setPosition(Gdx.graphics.getWidth()/2-title.getWidth()/2, Gdx.graphics.getHeight()-(title.getHeight()*2));
		
		TextButton playButton = new TextButton("Play", textButtonStyle);
		playButton.setWidth(Gdx.graphics.getWidth()/5);
		playButton.setHeight(Gdx.graphics.getHeight()/15);
		playButton.setPosition(Gdx.graphics.getWidth()/2-playButton.getWidth()/2,Gdx.graphics.getHeight()/2);
		
		TextButton creditsButton = new TextButton("Contact us", textButtonStyle);
		creditsButton.setWidth(Gdx.graphics.getWidth()/5);
		creditsButton.setHeight(Gdx.graphics.getHeight()/15);
		creditsButton.setPosition(Gdx.graphics.getWidth()/2-creditsButton.getWidth()/2,playButton.getY() - (creditsButton.getHeight()+(creditsButton.getHeight()/2)));
		
		stage.addActor(bgImage);
		stage.addActor(title);
		stage.addActor(playButton);
		stage.addActor(creditsButton);
		
		playButton.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				trigger(2);
			}
		});
		creditsButton.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				trigger(1);
			}
		});
	}
	
	private void trigger(int index){
		game.switchScreen(index);
	}
	
	/**
	 * Renders all objects on the screen once
	 * per frame.
	 */
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0.2f, 0.2f, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		stage.act(Math.min(delta, 1/60f));
		stage.draw();
	}
	
	/**
	 * Gets called if a resize happens.
	 * 
	 * Updates the viewport to match the new
	 * resolution.
	 */
	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height);
		BitmapFont tmp = skin.get("defaultFont", BitmapFont.class);
		tmp.setScale(scale);
	}

	/**
	 * Gets called when an application gets
	 * focus again.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void show() {
	}

	/**
	 * Gets called when an application loses
	 * focus.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void hide() {
	}
	
	/**
	 * Gets called when an application is paused.
	 * 
	 * I.e loses focus due to phone call or popup
	 * or similar.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void pause() {
	}
	
	/**
	 * Gets called when the application resumes
	 * after a pause.
	 * 
	 * Does nothing at the moment.
	 */
	@Override
	public void resume() {
	}
	
	/**
	 * Disposes of objects used in the screen to
	 * free up memory.
	 */
	@Override
	public void dispose() {
		skin.dispose();
		stage.dispose();
	}

}
